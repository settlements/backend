<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class Filters
{
    public function apply(Builder $query, array $filters)
    {
        foreach ($filters as $filter => $value) {
            $this->applyFilter($query, $filter, $value);
        }

        return $query;
    }

    protected function applyFilter(Builder $query, $filter, $value)
    {
        $filterMethod = 'filterBy' . ucfirst($filter);
        if (method_exists($this, $filterMethod)) {
            $this->$filterMethod($query, $value);
        } else {
            $query->where($filter, $value);
        }
    }


    protected function filterByPriceRub(Builder $query, $values)
    {
        $query->whereHas('housePrices', function ($subQuery) use ($values) {
            $subQuery->where('currency_id', 1)
                ->whereBetween('price', $values);
        });
    }

    protected function filterByPriceUsdt(Builder $query, $values)
    {
        $query->whereHas('housePrices', function ($subQuery) use ($values) {
            $subQuery->where('currency_id', 2)
                ->whereBetween('price', $values);
        });
    }

    protected function filterByFloors(Builder $query, $values)
    {
        $query->whereBetween('floors', $values);
    }

    protected function filterBySquare(Builder $query, $values)
    {
        $query->whereBetween('square', $values);
    }

    protected function filterByHouseType(Builder $query, $value)
    {
        $query->where('house_type_id', $value);
    }

    protected function filterBySettlement(Builder $query, $value)
    {
        $query->where('settlement_id', $value);
    }

    protected function filterByBedrooms(Builder $query, $values)
    {
        $query->whereBetween('bedrooms', $values);
    }

}