<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HouseType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType query()
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HouseType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HouseType extends BaseModel
{
    use HasFactory;
}
