<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HousePrice
 *
 * @property int $house_id
 * @property int $currency_id
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\HousePriceFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice whereHouseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HousePrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HousePrice extends BaseModel
{
    use HasFactory;
    protected $fillable = ['price', 'currency_id', 'house_id'];

    public static function find($id, $columns = ['*'])
    {
        return static::where('house_id', $id)->get($columns);
    }

}
