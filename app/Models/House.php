<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\House
 *
 * @property int $id
 * @property string $name
 * @property float $square
 * @property int $floors
 * @property int $bedrooms
 * @property int $house_type_id
 * @property int $settlement_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\HouseFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|House newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|House newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|House query()
 * @method static \Illuminate\Database\Eloquent\Builder|House whereBedrooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereFloors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereHouseTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSettlementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSquare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereUpdatedAt($value)
 * @property string $photo
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\HousePrice> $housePrices
 * @property-read int|null $house_prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|House wherePhoto($value)

 * @mixin \Eloquent
 */
class House extends BaseModel
{
    use HasFactory;

    protected $fillable = ['name', 'square','bedrooms','photo','house_type_id','settlement_id','currency'];
    public function housePrices()
    {
        return $this->hasMany(HousePrice::class);
    }

}
