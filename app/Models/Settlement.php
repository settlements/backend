<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * App\Models\Settlement
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property float $square
 * @property string $phone
 * @property string $youtube
 * @property string $presentation
 * @property string $photo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\SettlementFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement query()
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement wherePresentation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereSquare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settlement whereYoutube($value)
 * @mixin \Eloquent
 */
class Settlement extends BaseModel
{
    use HasFactory;
}

