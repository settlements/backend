<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filters\Filters;

class BaseController extends Controller
{
    protected $model;

    public function show($id)
    {
        $record = $this->model::find($id);

        if (!$record) {
            return response()->json(['message' => $this->getModelName() . ' not found'], 404);
        }
        if (method_exists($this, 'withRelationships')) {
            $record->load($this->withRelationships());
        }

        return response()->json($record);
    }

    public function index(Request $request, Filters $filters)
    {

        $records = $this->model::query();


        if (!empty($request->all())) {
            $records = $filters->apply($records, $request->all());
        }
        if (method_exists($this, 'withRelationships')) {
            $records->with($this->withRelationships());
        }

        $records = $records->get();

        return response()->json($records);
    }

    protected function getModelName()
    {
        return class_basename($this->model);
    }
}
