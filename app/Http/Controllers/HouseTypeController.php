<?php

namespace App\Http\Controllers;

use App\Models\HouseType;
use Illuminate\Http\Request;


class HouseTypeController extends BaseController
{
    protected $model = HouseType::class;
}
