<?php

namespace App\Http\Controllers;

use App\Models\HousePrice;
use Illuminate\Http\Request;

class HousePriceController extends BaseController
{
    protected $model = HousePrice::class;

    public function show($id)

    {

        $record = $this->model::find($id);



        if (!$record) {

            return response()->json(['message' => $this->getModelName() . ' not found'], 404);

        }



        return response()->json($record);

    }
}
