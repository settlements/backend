<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends BaseController
{
    protected $model = Gallery::class;


    public function saveFiles(Request $request, $id)
    {
        $savesFile = [];
        $allFilesForHouse = Gallery::get()->where('house_id', $id);
        foreach ($allFilesForHouse as $item) {
            $filePath = $item->path;
            \Storage::delete($filePath);
            $item->delete();
        }
        foreach ($request->file('files') as $file) {
            // Генерируем уникальное имя файла
            $filename = $file->getFilename() . '.' . $file->extension();
            $file->storeAs("public/houses/${id}", $filename);
            $filePath = \Storage::path("houses/${id}/${filename}");
            $filePathUri =url("storage/houses/${id}/${filename}");
            $savesFile[] = Gallery::create(['house_id' => $id, 'path' => $filePathUri]);
        }
        return response()->json($savesFile);
    }
}
