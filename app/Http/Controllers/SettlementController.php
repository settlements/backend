<?php

namespace App\Http\Controllers;

use App\Models\Settlement;

class SettlementController extends BaseController
{
    protected $model = Settlement::class;
}
