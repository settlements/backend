<?php

namespace App\Http\Controllers;

use App\Filters\Filters;
use App\Models\House;
use App\Models\HousePrice;
use Illuminate\Http\Request;

class FilterValueController extends Controller
{
    public function index()
    {
        $filters = [];
        $currency = HousePrice::whereIn('currency_id', [1, 2])
            ->groupBy('currency_id')
            ->select('currency_id', \DB::raw('MIN(price) as min_price'), \DB::raw('MAX(price) as max_price'))
            ->get();
        $house = House::select(['square', 'floors', 'bedrooms'])
            ->get();
        $houseData = [
            'square' => [
                'min' => $house->min('square'),
                'max' => $house->max('square'),
            ],
            'floors' => [
                'min' => $house->min('floors'),
                'max' => $house->max('floors'),
            ],
            'bedrooms' => [
                'min' => $house->min('bedrooms'),
                'max' => $house->max('bedrooms'),
            ],
        ];
        return response()->json([
            'currency' => $currency,
            'house' => $houseData,
        ]);
    }
}
