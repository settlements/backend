<?php

namespace App\Http\Controllers;

use App\Models\House;
use Illuminate\Http\Request;

class HouseController extends BaseController
{
    protected $model = House::class;

    protected function withRelationships()
    {
        return [
            'housePrices' => function ($query) {
                $query->select('house_id', 'currency_id', 'price');
            },
        ];
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'floors' => 'required',
            'square' => 'required',
            'house_type' => 'required',
            'settlement' => 'required',
            'bedrooms' => 'required',
        ]);
        var_dump($validatedData);

//        $house = House::create($validatedData);
//
//        return response()->json($house, 201);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'currency' => 'required',
            'price_rub' => 'required',
            'price_usdt' => 'required',
            'floors' => 'required',
            'square' => 'required',
            'house_type' => 'required',
            'settlement' => 'required',
            'bedrooms' => 'required',
        ]);


        $house = House::findOrFail($id);
        $house->update($validatedData);

//        dd($house->housePrices());
        $house->housePrices()->updateOrCreate(
            ['house_id' => $house->id, 'currency_id' => 1],
            ['price' => $validatedData['price_rub']]
        );
        $house->housePrices()->updateOrCreate(
            ['house_id' => $house->id, 'currency_id' => 2],
            ['price' => $validatedData['price_usdt']]
        );
//
        return response()->json($house);
    }

    public function destroy($id)
    {
        $house = House::findOrFail($id);
        $house->delete();

        return response()->json(null, 204);
    }

}
