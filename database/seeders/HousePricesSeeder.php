<?php

namespace Database\Seeders;

use App\Models\HousePrice;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HousePricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        HousePrice::factory()->count(10)->create();
    }
}
