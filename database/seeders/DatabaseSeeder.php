<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Gallery;
use App\Models\House;
use App\Models\HousePrice;
use App\Models\Settlement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CurrenciesSeeder::class,
            HouseTypesSeeder::class,
        ]);
        $housesTypes = DB::table('house_types')->get();
        $settlements = Settlement::factory()->count(3)->create();
        House::factory()
            ->count(10)
            ->state([
                'house_type_id' => $housesTypes->random()->id,
                'settlement_id' => $settlements->random()->id,
            ])->create();


        $houseIds = DB::table('houses')->latest()->limit(10)->pluck('id')->toArray();
        $currencies = DB::table('currencies')->select('id', 'rate')->get();
        foreach ($houseIds as $houseId) {
            $price = fake()->randomFloat(8, 300000, 99999999);
            foreach ($currencies as $currency) {
                HousePrice::factory()->create([
                    'house_id' => $houseId,
                    'currency_id' => $currency->id,
                    'price' => round($price / $currency->rate, 3)
                ]);
            }
        }


        Gallery::factory()->count(30)->create();
    }
}
