<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HouseTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('house_types')->insert(
            [
                ['name' => 'Дом'],
                ['name' => 'Коттедж'],
                ['name' => 'Таунхаус'],
                ['name' => 'Квартира'],
            ]
        );
    }
}
