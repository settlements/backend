<?php

namespace Database\Seeders;

use App\Models\House;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $housesTypes = DB::table('house_types')->get();
        $settlements = DB::table('settlements')->get();

        House::factory()->count(10)->state([
            'house_type_id' => $housesTypes->random()->id,
            'settlement_id' => $settlements->random()->id,
        ])->create();
    }
}
