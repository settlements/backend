<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('house_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('house_id')->references('id')->on('houses');
            $table->foreignId('currency_id')->references('id')->on('currencies');
            $table->float('price',15,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('house_prices');
    }
};
