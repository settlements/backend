<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->float('square');
            $table->integer('currency');
            $table->integer('floors',false,true);
            $table->integer('bedrooms',false,true);
            $table->string('photo',100);
            $table->foreignId('house_type_id')->references('id')->on('house_types');
            $table->foreignId('settlement_id')->references('id')->on('settlements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('houses');
    }
};
