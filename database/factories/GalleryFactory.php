<?php

namespace Database\Factories;

use App\Models\House;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Gallery>
 */
class GalleryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $house = DB::table('houses')->latest()->limit(10)->get();
        $houseId = $house->isEmpty() ? House::factory()->create()->id : $house->random()->id;
        return [
            'house_id' => $houseId,
            'path' => fake()->imageUrl(640,480,true)
        ];
    }
}
