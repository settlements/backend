<?php

namespace Database\Factories;

use App\Models\House;
use App\Models\HousePrice;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\HousePrice>
 */
class HousePriceFactory extends Factory
{
    protected $model = HousePrice::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
            $house = DB::table('houses')->pluck('id')->toArray();
            $currency = DB::table('currencies')->pluck('id')->toArray();
            return [
                'house_id' => fake()->randomElement($house),
                'currency_id' => fake()->randomElement($currency),
                'price' => fake()->randomFloat(6,12000,999999)
            ];
    }
}
