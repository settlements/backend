<?php

namespace Database\Factories;

use App\Models\House;
use Database\Seeders\HouseTypesSeeder;
use Database\Seeders\SettlementsSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\House>
 */
class HouseFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $housesTypes = DB::table('house_types')->get();
        $settlement = DB::table('settlements')->limit(10)->get();
        $housesTypesId = $housesTypes->isEmpty() ? '' : $housesTypes->random()->id;
        $settlementId = $settlement->isEmpty() ? '' : $settlement->random()->id;
        return [
            'name' => fake()->streetName(),
            'square' => fake()->randomFloat(2, 30, 300),
            'floors' => rand(1, 14),
            'currency' => rand(1,2),
            'bedrooms' => rand(1, 7),
            'house_type_id' => $housesTypesId, //neeed reference to house_types
            'settlement_id' => $settlementId,//neeed reference to settlements
            'photo' => fake()->imageUrl(640,480,true),
        ];
    }
}
