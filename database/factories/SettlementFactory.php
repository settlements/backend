<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class SettlementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
                'name' => fake()->city(),
                'address' => fake()->streetAddress(),
                'square' => fake()->randomFloat(2,70,999),
                'phone' => substr(fake()->e164PhoneNumber(),1),
                'youtube' => fake()->youtubeUri(),
                'presentation' => 'path/to/file'.rand(1,3).'.pdf',
                'photo' => fake()->imageUrl(640,480,true),
        ];
    }
}



