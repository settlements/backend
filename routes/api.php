<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\HouseController;
use App\Http\Controllers\SettlementController;
use App\Http\Controllers\CurrencyController;
use \App\Http\Controllers\HousePriceController;
use \App\Http\Controllers\GalleryController;
use \App\Http\Controllers\HouseTypeController;
use \App\Http\Controllers\FilterValueController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::resource('houses', HouseController::class);
Route::get('/house_filters', [FilterValueController::class, 'index']);
Route::get('/house_types', [HouseTypeController::class, 'index']);
Route::get('/settlements/{id}', [SettlementController::class, 'show']);
Route::get('/settlements', [SettlementController::class, 'index']);
Route::get('/currencies/{id}', [CurrencyController::class, 'show']);
Route::get('/currencies', [CurrencyController::class, 'index']);
Route::get('/house_prices/{house_id}', [HousePriceController::class, 'show']);
Route::get('/house_prices', [HousePriceController::class, 'index']);
Route::get('/gallery/{house_id}', [GalleryController::class, 'show']);
Route::post('/gallery/{house_id}', [GalleryController::class, 'saveFiles']);
Route::put('/gallery/{house_id}', [GalleryController::class, 'saveFiles']);
